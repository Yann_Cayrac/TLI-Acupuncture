#! /bin/bash

# Selection du type d'installation
echo "Quel mode voulez-vous lancer? [ S = scaled / M = minimal (par defaut) ]"
read RESP

echo "=> Build de l'image docker WWW"
docker build -t tli-www WWW

echo "=> Build de l'image docker DB"
docker build -t tli-db DB

echo "=> Creation si necessaire du volume pour le dashboard"
R=$(docker volume ls -q | grep portainer_data)
if [ "$?" != "0" ]
then
	docker volume create portainer_data
fi

echo "=> Creation si necessaire du volume pour WWW"
R=$(docker volume ls -q | grep www_data)
if [ "$?" != "0" ]
then
        docker volume create www_data
fi


echo "=> Nettoyage d'un ancien run"
docker service rm PMA DB WWW DASH
docker container rm -f PMA DB WWW DASH
docker network rm MINIMAL_BR OL_WWW OL_SQL
sleep 10

# Mise en place du mode scaled
if [ "$RESP" == "s" ] || [ "$RESP" == "S" ]
then
        echo " => Configuration des networks..."
        docker network create -d overlay OL_WWW
        docker network create -d overlay OL_SQL
        echo " => Lancement du service WWW..."
        docker service create --name WWW --mount type=volume,src=www_data,dst=/var/www/html  --network OL_WWW --network OL_SQL --publish 80:80 --replicas=10 tli-www
        echo " => Lancement du service DB..."
	docker service create --name DB --network OL_SQL -e MYSQL_ROOT_PASSWORD=root --publish 3306:3306 --replicas=1 tli-db
        echo " => Lancement du service PMA..."
	docker service create --name PMA --network OL_SQL -e PMA_HOST=DB --publish 8080:80 --replicas=1 phpmyadmin/phpmyadmin
	echo " => Lancement du service DASH..."
        docker service create --name DASH --constraint 'node.role == manager' --mount type=bind,src=//var/run/docker.sock,dst=/var/run/docker.sock --mount type=volume,src=portainer_data,dst=/data --publish 9000:9000 --replicas=1 portainer/portainer

# Mise en place du mode minimal
else
	echo " => Configuration des networks..."
	docker network create MINIMAL_BR
	echo " => Lancement de WWW..."
	docker run --name WWW --network MINIMAL_BR  -d -p 80:80 tli-www
	echo " => Lancement de DB..."
	docker run --name DB --network MINIMAL_BR -e MYSQL_ROOT_PASSWORD=root -d -p 3306:3306 tli-db
	echo " => Lancement de PMA..."
	docker run --name PMA --network MINIMAL_BR -e PMA_HOST=DB -d -p 8080:80 phpmyadmin/phpmyadmin
	echo " => Lancement de DASH..."
        docker run --name DASH -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data -d -p 9000:9000 portainer/portainer
fi

