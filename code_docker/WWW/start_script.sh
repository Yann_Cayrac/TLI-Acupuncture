#! /bin/bash

# Recuperation des fichiers du site sur le gitlab
rm -Rf /var/www/html/*
wget --no-check-certificate http://www.gitlab.com/Yann_Cayrac/TLI-Acupuncture/-/archive/master/TLI-Acupuncture-master.zip
unzip  TLI-Acupuncture-master.zip -d .
mv TLI-Acupuncture-master/code_web/*  /var/www/html
cp -f TLI-Acupuncture-master/code_web/.htaccess  /var/www/html
rm -Rf TLI-Acupuncture-master*
sed -i s/localhost/DB/g /var/www/html/Config.php
chown www-data:www-data -R /var/www/html

# Lancement du serveur apache
/usr/local/bin/apache2-foreground
