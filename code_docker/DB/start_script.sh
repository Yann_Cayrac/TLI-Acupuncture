#! /bin/bash

# Telechargement de la BD
wget --no-check-certificate http://www.gitlab.com/Yann_Cayrac/TLI-Acupuncture/-/archive/master/TLI-Acupuncture-master.zip
unzip  TLI-Acupuncture-master.zip -d /

# Import programmee de la BD
bash -c 'while true; do sleep 5; mysql -u root -proot -h 127.0.0.1 -D mysql -e "show databases;" > /dev/null; [ "$?" -ne "0" ] || break; done; sleep 5; mysql -u root -proot  < TLI-Acupuncture-master/code_db/database.sql; rm -Rf /TLI-Acupuncture-master*' &

# Demarrage mariadb
/usr/local/bin/docker-entrypoint.sh mysqld
