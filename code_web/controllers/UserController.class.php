<?php
require_once("Controller.php");
require_once ("./librairies/repository/UserRepository.php");
//require_once ("./librairies/entity/User.php");

class UserController extends Controller {

    public function login()
    {
         if(isset($_POST) && !empty($_POST)) {
            $email = $this->validateInput($_POST['loginEmail']);
            $pwd = $this->validateInput($_POST['loginPwd']);

             $userRepository = new UserRepository();
             $user = $userRepository->findByEmail($email);

             if($user != null){
                 if(password_verify($pwd, $user->getPwd())) {
                     $_SESSION['user'] = $user->getIdU();
                     $_SESSION['nom'] = $user->getNom();
                     $_SESSION['prenom'] = $user->getPrenom();
                 }
             }
         }

         header("Location:/");
    }

    public function inscription()
    {
        if(isset($_POST) && !empty($_POST)){
            $user = new User();
            $user->setPrenom($this->validateInput($_POST['firstName']));
            $user->setNom($this->validateInput($_POST['lastName']));
            $user->setEmail($this->validateInput($_POST['email']));
            $user->setPwd(password_hash($this->validateInput($_POST['password']), PASSWORD_BCRYPT));
            $pwdConfirm = $this->validateInput($_POST['passwordConfirm']);

            if(password_verify($pwdConfirm, $user->getPwd())) {
                $userRepository = new UserRepository();
                $userRepository->create($user);
                $_SESSION['user'] = $userRepository->findByEmail($user->getEmail())->getIdU();
                $_SESSION['nom'] = $user->getNom();
                $_SESSION['prenom'] = $user->getPrenom();
                header("Location:/");
            } else {
                $this->smarty->assign(array(
                    "errorMessage" => "Les mots de passe doivent être identiques"
                ));
                $this->smarty->display("views/html/inscription.html");
            }
        } else {
            $this->smarty->display("views/html/inscription.html");
        }
    }

    public function logout(){
        session_destroy();
        header("Location:/");
    }

}
?>
