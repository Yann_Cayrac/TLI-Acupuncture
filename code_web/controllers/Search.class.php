<?php
require_once("Controller.php");
require_once ("./librairies/repository/PathologieRepository.php");

class Search extends Controller {

    public function index()
    {
       if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['searchInput'])) {
                $searchInput = $this->validateInput($_POST['searchInput']);

                $pathoRepository = new PathologieRepository();

                $pathos = $pathoRepository->findByKeyword($searchInput);

                $this->smarty->assign(array(
                    "pathos" => $pathos,
                    "searchInput" => $searchInput
                ));
            }
        }
        $this->smarty->display("views/html/recherche.html");
    }
}
?>