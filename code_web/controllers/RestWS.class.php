<?php
require_once("./librairies/smarty/Smarty.class.php");
require_once("./librairies/repository/PathologieRepository.php");

class RestWS {
    public function pathologiesXML()
    {
        header('Content-Type: application/xml');

        $pr = new PathologieRepository();
        $pathos = $pr->findAll();

        $xml = new SimpleXMLElement("http://tli.com/public/xml/pathologies.xml", NULL, TRUE);
        
        $pathosNode = $xml->xPath('/pathologies')[0];

        foreach ($pathos as $p) {
            $pathoNode = $pathosNode->addChild('pathologie');
            $pathoNode->addChild('meridien', $p->getMer());
            $pathoNode->addChild('type', $p->getType());
            $pathoNode->addChild('description', $p->getDesc());
        }

        echo $xml->asXML();
    }

    public function calcul()
    {
        $params = func_get_args();
        $ret = 0;

        header('Content-Type: application/json');
        
        switch ($params[0]) {
            case "addition":
                $ret = $this->addition($params[1], $params[2]);
                break;
            case "soustraction":
                $ret = $this->soustraction($params[1], $params[2]);
                break;
            case "multiplication":
                $ret = $this->multiplication($params[1], $params[2]);
                break;
            case "division":
                $ret = $this->division($params[1], $params[2]);
                break;
            default:
                $this->returnError('unknown operation', $params[0]);
        }

        echo json_encode(array('result' => $ret));
    }

    private function returnError($message, $explanation)
    {
        http_response_code(400); // bad request
        echo json_encode(array('error' => $message.' : '.$explanation));
        die();
    }

    private function addition($number1, $number2)
    {
        return $number1 + $number2;
    }

    private function soustraction($number1, $number2)
    {
        return $number1 - $number2;
    }

    private function multiplication($number1, $number2)
    {
        return $number1 * $number2;
    }

    private function division($number1, $number2)
    {
        if ($number2 == 0) {
            $this->returnError('cannot divide by', $number2);
        }

        return $number1 / $number2;
    }
}
?>