<?php

require_once("./librairies/smarty/Smarty.class.php");

class Controller
{
    public function __construct()
    {
        $this->user = null;
        $this->smarty = new Smarty();

        $this->smarty->assign(array(
            "user" => $this->user
        ));
    }


    protected function validateInput($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

}