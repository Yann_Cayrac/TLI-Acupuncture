<?php
require_once("Controller.php");

class Home extends Controller {
    public function index()
    {
        $this->smarty->display("views/html/accueil.html");
    }

    public function about()
    {
        $this->smarty->display("views/html/apropos.html");
    }
}
?>