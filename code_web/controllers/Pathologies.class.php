<?php
require_once("Controller.php");
require_once ("./librairies/repository/PathologieRepository.php");

class Pathologies extends Controller {
    public function index()
    {
		$pathologies = $this->getAllPathologies();
		$this->smarty->assign(array(
			"pathologies" => $pathologies,
			"types" => $this->getTypes(),
			"meridiens" => $this->getMeridiens()
		));
		
        $this->smarty->display("views/html/pathologies.html");
    }

    public function detail()
    {
        // pour récupérer les params
        $params = func_get_args();
        $pathoId = intval($params[0]);

        if(is_int($pathoId)){
            $pathoRepository = new PathologieRepository();
            $patho = $pathoRepository->findById($pathoId);

            if($patho == null) {
                $this->smarty->assign(array(
                    "errorMessage" => "La pathologie demandée n'existe pas"
                ));
            } else {
                $this->smarty->assign(array(
                    "patho" => $patho
                ));
            }
        } else {
            $this->smarty->assign(array(
                "errorMessage" => "La pathologie demandée n'existe pas"
            ));
        }

        $this->smarty->display("views/html/pathologieDetail.html");
    }
    
    public function getAllPathologies() {
			$repoPatho = new PathologieRepository();
			
			return $repoPatho->findAll();
	}
	
	public function getTypes() {
			$repoPatho = new PathologieRepository();
			
			return $repoPatho->findTypes();
	}
	
	public function getMeridiens() {
			$repoPatho = new PathologieRepository();
			
			return $repoPatho->findMeridiens();
	}
}
?>
