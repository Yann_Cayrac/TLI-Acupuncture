<?php
    require_once './librairies/router/Route.class.php';
    require_once './librairies/router/Router.class.php';

    session_save_path(rtrim($_SERVER['DOCUMENT_ROOT'], '/') . "/session");
    session_start();

    $router = new Router(array(
        new Route('/', 'Home::index'),
        new Route('/pathologies', 'Pathologies::index'),
        new Route('/pathologie/:id', 'Pathologies::detail'),
        new Route('/login', 'UserController::login'),
        new Route('/logout', 'UserController::logout'),
        new Route('/inscription', 'UserController::inscription'),
        new Route('/search', 'Search::index'),
        new Route('/about', 'Home::about'),
        new Route('/paint', 'Paint::index'),
        new Route('/webservices/calculatrice/:operation/:nb1/:nb2', 'RestWS::calcul'),
        new Route('/webservices/pathologies', 'RestWS::pathologiesXML'),
    ), '');

    $router->run();
?>
