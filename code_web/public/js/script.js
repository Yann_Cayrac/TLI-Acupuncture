
function toggleLoginForm(){
 var modal = document.getElementById("login-form");
 if(modal){
     modal.classList.toggle("open");
     if(modal.classList.contains("open")){
         modal.getElementsByTagName("input")[0].focus();
     }
 }
}

function toggleMenu(button){
    button.classList.toggle("expanded");

    var menu = document.getElementsByTagName("nav")[0];

    if(menu){
        menu.classList.toggle("open");
    }
}