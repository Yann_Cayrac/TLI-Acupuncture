const root = document.documentElement;

var can = document.getElementById('canvas');
var context = can.getContext("2d");

var widthInput = document.getElementById("pen-width-input");
var colorInput = document.getElementById("pen-color-input");

var pressed = false;
var drawingLine = false;
var drawingRect = false;

var lx1,ly1,lx2,ly2;
var penForm = "round";

function init(){
    console.log("INIT");
    setWidth(widthInput);
    setColor(colorInput);
}

function clearCanvas(){
    context.clearRect(0,0,can.width, can.height);
}

function generateRandColor(){
    return "rgb(" + (Math.floor((Math.random() * 255) + 1)) + ", " + (Math.floor((Math.random() * 255) + 1)) + ", " + (Math.floor((Math.random() * 255) + 1)) + ")";
}

function setColor(colorForm){
    var color = colorForm.value;
    console.log(color);
    context.strokeStyle = color;
    context.fillStyle = color;

    root.style.setProperty('--color', color);
}

function setWidth(range){
    context.lineWidth = range.value;
    var penDisplay = document.getElementById("pen-display");

    root.style.setProperty('--width', range.value + "px");
}

function setForm(form){
    penForm = form;
    var penDisplay = document.getElementById("pen-display");
    penDisplay.className = form;
}

function drawPoint(x,y){
    if(penForm == "round"){
        context.beginPath();
        context.arc(x+context.lineWidth/4, y+context.lineWidth/4, context.lineWidth/2, 0, 2*Math.PI);
        context.fill();
    }
    else if (penForm == "square") {
        context.fillRect(x,y, context.lineWidth, context.lineWidth);
    } else {
        context.beginPath();
        context.arc(x, y, context.lineWidth, 0, 2*Math.PI);
        context.fill();
    }
}

function drawRectangle(x1, y1, x2, y2){
    var w = context.lineWidth;
    context.fillRect(x1-w, y1-w,x2-x1+w,y2-y1+w);
}



/*================== EVENT LISTENERS ==================*/

can.addEventListener("mousedown", function(e){
    pressed = true;
    console.log(e.clientY + "  - " + can.offsetTop);
    var x = e.clientX - can.offsetLeft;
    var y = e.clientY - can.offsetTop + window.scrollY;
    var stroke = context.lineWidth;
    console.log("y : " + y);

    if(drawingLine){
        context.beginPath();
        context.moveTo(lx1, ly1);
        context.lineTo(x, y);
        context.stroke();
    } else if (drawingRect) {
        drawRectangle(lx1, ly1, x, y);
    } else {
        drawPoint(x-stroke/2,y-stroke/2,stroke,stroke);
    }

    lx1 = x;
    ly1 = y;

});

can.addEventListener("mousemove", function(e){
    if(pressed){
        var x = e.clientX - can.offsetLeft;
        var y = e.clientY - can.offsetTop + window.scrollY;
        var stroke = context.lineWidth;

        if(!drawingLine && !drawingRect){
            lx1 = x;
            ly1 = y;
            drawPoint(x-stroke/2,y-stroke/2,stroke,stroke);
        }
    }
});

can.addEventListener("mouseup", function(e) {
    pressed = false;
})


document.addEventListener("keydown", function(e){
    if(e.key == "Shift"){
        drawingLine = true;
    } else if (e.code == "KeyR") {
        drawingRect = true;
    }
});
document.addEventListener("keyup", function(e){
    if(e.key == "Shift"){
        drawingLine = false;
    } else if (e.code == "KeyR") {
        drawingRect = false;
    }
});

window.onload = init();
