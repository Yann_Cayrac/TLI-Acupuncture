<?php
require_once "AbstractRepository.php";
require_once ("./librairies/entity/User.php");

class UserRepository extends AbstractRepository {

    public function __construct()
    {
        parent::__construct();
        $this->table = "users";
    }

    public function findById($id){
        $query = $this->db->prepare("SELECT * FROM " . $this->table . " WHERE idU = ?");
        $query->execute(array($id));

        $user = new User();
        $res = $query->fetch();

        $user->setIdU($res['idU']);
        $user->setEmail($res['email']);
        $user->setPwd($res['pwd']);
        $user->setNom($res['nom']);
        $user->setPrenom($res['prenom']);

        return $user;
    }

    public function findByEmail($email){
        $query = $this->db->prepare("SELECT * FROM " . $this->table . " WHERE email = ?");
        $query->execute(array($email));

        $user = new User();
        $res = $query->fetch();

        $user->setIdU($res['idU']);
        $user->setEmail($res['email']);
        $user->setPwd($res['pwd']);
        $user->setNom($res['nom']);
        $user->setPrenom($res['prenom']);

        return $user;
    }

    public function create($entity){
        if($this->validate($entity)){
            $query = $this->db->prepare("INSERT INTO " . $this->table . " (email, pwd, nom, prenom) VALUES (?, ?, ? ,?)");
            return $query->execute(array($entity->getEmail(),$entity->getPwd(),$entity->getNom(),$entity->getPrenom()));
        }
    }

    public function remove($entity){
        $query =$this->db->prepare("DELETE FROM " . $this->table . " WHERE idU = ?");
        return $query->execute(array($entity->getIdU()));
    }

    public function update($entity){
        if($this->validate($entity)){
            $query = $this->db->prepare("UPDATE " . $this->table . " SET email = ?, pwd = ?, nom = ?, prenom = ? WHERE idU = ?");
            return $query->execute(array($entity->getEmail(),$entity->getPwd(),$entity->getNom(),$entity->getPrenom(), $entity->getIdU()));
        }
    }

    public function validate($entity){
        if(get_class($entity) != 'User') return false;
        if(!$entity->getPrenom()) return false;
        if(!$entity->getPwd()) return false;
        if(!$entity->getNom()) return false;
        if(!$entity->getEmail()) return false;
        return true;
    }
}

?>