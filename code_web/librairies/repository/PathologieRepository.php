<?php

require_once("AbstractRepository.php");
require_once("./librairies/entity/Pathologie.php");

class PathologieRepository extends AbstractRepository {
    public function __construct()
    {
        parent::__construct();
        $this->table = 'patho';
    }

    public function findAll()
    {
        $pathos = array();

        $res = parent::findAll();

        foreach ($res as $row){
            $p = new Pathologie();
            $p->setIdP($row['idP']);
            $p->setDesc($row['desc']);
            $p->setMer($row['mer']);
            $p->setType($row['type']);
            array_push($pathos, $p);
        }

        return $pathos;
    }

    public function findById($id)
    {
        $query = $this->db->prepare("SELECT * FROM " . $this->table . " WHERE idP = ?");
        $query->execute(array($id));
        $row = $query->fetch();

        if($row['idP']) {
            $p = new Pathologie();
            $p->setIdP($row['idP']);
            $p->setDesc($row['desc']);
            $p->setMer($row['mer']);
            $p->setType($row['type']);
        } else {
            $p = null;
        }

        return $p;
    }

    public function findByKeyword($keyword){
        $pathos = array();

        $sql = "SELECT p.idP, p.desc, p.mer, p.type FROM keywords as kw 
                         INNER JOIN keySympt as ks on kw.idK = ks.idK
                         INNER JOIN symptPatho as sp on ks.idS = sp.idS 
                         INNER JOIN patho as p on sp.idP = p.idP 
                         WHERE kw.name = ?";

        $query = $this->db->prepare($sql);
        if($query->execute(array($keyword))) {
            foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $row) {
                $p = new Pathologie();
                $p->setIdP($row['idP']);
                $p->setDesc($row['desc']);
                $p->setMer($row['mer']);
                $p->setType($row['type']);
                array_push($pathos, $p);
            }
        }

        $query = null;
        return $pathos;
    }
    
    public function findTypes() {
		$query = $this->db->prepare("SELECT DISTINCT type FROM " . $this->table);
        $query->execute();
        $row = $query->fetchAll();
        return $row;
	}
	
	public function findMeridiens() {
		$query = $this->db->prepare("SELECT DISTINCT mer FROM " . $this->table);
        $query->execute();
        $row = $query->fetchAll();
        return $row;
	}

}

?>
