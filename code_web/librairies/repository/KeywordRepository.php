<?php

require_once "AbstractRepository.php";
class KeywordRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'keywords';
    }

    public function findById($id)
    {
        $statement = $this->db->prepare('SELECT * FROM '.$this->table.' WHERE idK = ?');
        $statement->execute(array($id));
        $ret = $statement->fetch(PDO::FETCH_ASSOC);
        $statement = null;

        $k = new Keyword();
        $k->setIdk($ret['idK']);
        $k->setName($ret['name']);

        return $k;
    }


}
?>