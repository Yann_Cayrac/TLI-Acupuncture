<?php
require_once "AbstractRepository.php";

class MeridienRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'meridien';
    }

    public function findById($id)
    {
        $statement = $this->db->prepare('SELECT * FROM '.$this->table.' WHERE code = ?');
        $statement->execute(array($id));
        $ret = $statement->fetch(PDO::FETCH_ASSOC);
        $statement = null;

        $m = new Meridien();
        $m->setCode($ret['code']);
        $m->setElement($ret['element']);
        $m->setNom($ret['nom']);
        $m->setYin($ret['yin']);

        return $m;
    }
}
?>