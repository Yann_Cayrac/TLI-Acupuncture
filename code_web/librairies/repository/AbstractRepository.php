<?php

require_once ("./librairies/pdo/PdoConnection.php");

abstract class AbstractRepository {
    protected $table;
    protected $db;

    public function __construct()
    {
        try {
            $this->db = PdoConnection::getInstance();
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function findAll()
    {
        return $this->db->query('SELECT * FROM '.$this->table);
    }

    abstract public function findById($id); // abstract car le champs id n'est pas uniforme
}
?>