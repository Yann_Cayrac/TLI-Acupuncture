<?php
require_once "AbstractRepository.php";
require_once ("./librairies/entity/Symptome.php");

class SymptomeRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'symptome';
    }

    public function findById($id)
    {
        $statement = $this->db->prepare('SELECT * FROM '.$this->table.' WHERE idS = ?');
        $statement->execute(array($id));
        $ret = $statement->fetch(PDO::FETCH_ASSOC);
        $statement = null;

        $s = new Symptome();
        $s->setIdS($ret['idS']);
        $s->setDesc($ret['desc']);

        return $s;
    }
}
?>