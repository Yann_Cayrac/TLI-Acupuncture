<?php

require_once ("./Config.php");

class PdoConnection
{
    private static $instance = null;

    private function __construct(){}

    public static function getInstance(){
        if(self::$instance == null){
            self::$instance = new PDO('mysql:host='.Config::HOST.';dbname='.Config::DBNAME, Config::USERNAME, Config::PWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        }
        return self::$instance;
    }

    private function __destruct()
    {
        self::$instance = null;
    }
}