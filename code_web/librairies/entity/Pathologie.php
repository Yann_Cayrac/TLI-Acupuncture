<?php

require_once ("AbstractEntity.php");
require_once  ("./librairies/repository/SymptomeRepository.php");

class Pathologie extends AbstractEntity {

    protected $idP;
    protected $mer;
    protected $type;
    protected $desc;

    protected $db;

    public function __construct(){
        try {
            $this->db = PdoConnection::getInstance();
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    public function getSymptomes(){
        $query = $this->db->prepare("SELECT idS FROM symptPatho WHERE idP = ?");
        $query->execute(array($this->idP));

        $res = $query->fetchAll();

        $symptRepository = new SymptomeRepository();
        $sympt = array();

        foreach($res as $row){
            array_push($sympt,$symptRepository->findById($row['idS']));
        }

        return $sympt;
    }

    /**
     * @return mixed
     */
    public function getIdP()
    {
        return $this->idP;
    }

    /**
     * @param mixed $idP
     */
    public function setIdP($idP)
    {
        $this->idP = $idP;
    }

    /**
     * @return mixed
     */
    public function getMer()
    {
        return $this->mer;
    }

    /**
     * @param mixed $mer
     */
    public function setMer($mer)
    {
        $this->mer = $mer;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param mixed $desc
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;
    }



}