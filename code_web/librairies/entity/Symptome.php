<?php
require_once ("AbstractEntity.php");

class Symptome extends AbstractEntity
{
    protected $idS;
    protected $desc;

    /**
     * @return mixed
     */
    public function getIdS()
    {
        return $this->idS;
    }

    /**
     * @param mixed $idS
     */
    public function setIdS($idS)
    {
        $this->idS = $idS;
    }

    /**
     * @return mixed
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param mixed $desc
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;
    }


}