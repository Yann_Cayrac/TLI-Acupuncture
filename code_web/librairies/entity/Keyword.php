<?php
require_once ("AbstractEntity.php");

class Keyword extends AbstractEntity
{

    protected $idk;
    protected $name;

    /**
     * @return mixed
     */
    public function getIdk()
    {
        return $this->idk;
    }

    /**
     * @param mixed $idk
     */
    public function setIdk($idk)
    {
        $this->idk = $idk;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



}