<?php
require_once ("AbstractEntity.php");

class Meridien extends AbstractEntity {

    protected $code;
    protected $nom;
    protected $element;
    protected $yin;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * @param mixed $element
     */
    public function setElement($element)
    {
        $this->element = $element;
    }

    /**
     * @return mixed
     */
    public function getYin()
    {
        return $this->yin;
    }

    /**
     * @param mixed $yin
     */
    public function setYin($yin)
    {
        $this->yin = $yin;
    }

}