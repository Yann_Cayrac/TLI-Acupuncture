<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 5/22/18
 * Time: 9:36 PM
 */

class AbstractEntity
{
    protected $db;

    public function __construct(){
        $this->db = PdoConnection::getInstance();
    }

    public function __destruct()
    {
        $this->db = null;
    }

}