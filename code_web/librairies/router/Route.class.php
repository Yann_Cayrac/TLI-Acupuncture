<?php
/**
 * sous la forme : /symptome/:id
 */
class Route {

    private $url;
    private $callable;
    private $matches;

    public function __construct($url, $callable)
    {
        $this->url = trim($url, '/');  // On retire les / inutils
        $this->callable = $callable;
    }

    /**
    * Permettra de capturer l'url avec les paramètres
    **/
    public function match($requestUrl)
    {
        $requestUrl = trim($requestUrl, '/');
        $url = preg_replace_callback('#(:\w+)#', array($this, 'paramMatch'), $this->url);

        $regex = "#^$url$#";
        if(!preg_match($regex, $requestUrl, $matches)){
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;

        return true;
    }

    private function paramMatch($match)
    {
        if (isset($this->matches[$match[1]])) {
            return '(' . $this->matches[$match[1]] . ')';
        }
        return '([^/]+)';
    }

    public function call()
    {
        $callablesArray = explode('::', $this->callable);

        require_once('./controllers/' . $callablesArray[0] . '.class.php');
        
        $controller = new $callablesArray[0]();
        return call_user_func_array(array($controller, $callablesArray[1]), $this->matches);
    }
}
?>
