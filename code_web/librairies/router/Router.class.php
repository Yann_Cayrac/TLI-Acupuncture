<?php
require_once("./librairies/smarty/Smarty.class.php");

/**
 * Classe de lien entre les routes et les url
 */
class Router {

     private $routes = array();
     private $basePath = '';

    /**
    * @param array $routes
    */
    public function __construct($routes = array(), $basePath = '')
    {
        // Si $routes n'est pas itérable
        if (!is_array($routes) && !($routes instanceof Traversable)) {
            throw new \Exception("Routes should be an array or an iterable");
        }

        $this->routes = $routes;
        $this->basePath = $basePath;
    }

    public function run($requestUrl = null)
    {
        // Si l'url n'est pas renseignée
        if ($requestUrl === null) {
            $requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
        }

        // On ne conserve que la partie de la requête qui correspond à une route
        $requestUrl = substr($requestUrl, strlen($this->basePath));

        foreach ($this->routes as $route) {
            if ($route->match($requestUrl)) {
                return $route->call();
            }
        }

        // 404 si aucune route ne correspond
        http_response_code(404);
        $smarty = new Smarty();
        $smarty->display("views/html/404.html");
        die();
    }
}
?>
